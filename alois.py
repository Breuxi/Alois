import asyncio
import os
import random
import re
import sys
import json
import configparser
from datetime import datetime
from threading import Timer

import discord

__author__ = 'Breuxi'

"""
Markdown Link - [Name](URL)
"""

client = discord.Client()

# bestellung_re = re.compile(r'alois(,\s)?((bitte\s)?(gib\s)?(mir)?)|((ich|i)\s)?((will|wü\s)|(((hätte\s)?(möchte\s)?)(gern(e)?\s)?))((a|ein(en)?)\s)',

bestellung_re = re.compile(
    r'alois(,)?\s((bitte\s)?(gib\s)?(mir)?)|((kann\s)?(ich|i)\s)?((will|wü\s)|(((hätte|hab\s)?(möchte|lust\s)?)((gern(e))?\s|(bitte?\s))?))((a|ein(en)?|auf)\s)',
    re.IGNORECASE)

last_action = {
    'type': 'none',
    'object': ''
}

if os.path.exists('config.cfg'):
    config = configparser.ConfigParser()
    config.read("config.cfg")
    if config.has_section("Discord") and config.has_option("Discord", "token"):
        discord_token = config.get('Discord', 'token')
    else:
        print("Config unvollständig! Bitte Discord Token hinzufügen")
        sys.exit(1)
else:
    print("Die config.cfg Datei existiert nicht, bitte erstellen die eine nach dem Beispiel in config.cfg.example")
    sys.exit(1)

if os.path.exists('reaction.json'):
    with open('reaction.json', 'r', encoding="utf-8") as f:
        try:
            config = json.load(f)
        except json.JSONDecodeError:
            print("Json Error!")
else:
    print("Die reaction.json Datei existiert nicht")
    sys.exit(1)



@client.event
async def on_ready():
    print('Logged in as {} - {}'.format(client.user.name, client.user.id))
    print('------')
    for server in client.servers:
        print('- Name: {} | Id: {}'.format(server.name, server.id))
    print('------')
    await client.change_presence(game=discord.Game(name=" die Kneipe putzen..."))


@client.event
async def on_message(message):
    if message.author.id != client.user.id:

        match = bestellung_re.match(message.content)
        if match:
            # print("Match")
            await client.send_typing(message.channel)
            bestellung = message.content.lower()[match.end():].replace("!", "").replace("?", "").replace(".", "")
            if "speisekarte" in bestellung:
                description = ""
                gerichte = []
                getraenke = []
                for be in config["bestellungen"]:
                    if be["typ"] == "gericht":
                        gerichte.append(be)

                    if be["typ"] == "getraenk":
                        getraenke.append(be)

                description += "**Gerichte:**\n"

                for gericht in gerichte:
                    description += gericht["name"] + "\n"

                description += "\n**Getränke:**\n"

                for getraenk in getraenke:
                    description += getraenk["name"] + "\n"

                em = discord.Embed(color=discord.Colour.dark_green(), description=description)
                em.set_author(name="Alois's Speisekarte",
                              icon_url="https://images.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.lochner-verpackung.de%2Fmedia%2Fimages%2Fpopup%2F30399-5305664.jpg")

                em.set_footer(text="Henlo, i bims der Alois owo")

                await client.send_message(message.channel, content="Ah, du wuist die Spoisekorte. Hioa!", embed=em)

            else:
                for gericht in config["bestellungen"]:
                    if "regex" not in gericht:
                        gericht["regex"] = gericht["name"].lower()
                    if re.compile(gericht["regex"], re.IGNORECASE).search(bestellung):
                        await client.send_message(message.channel, random.choice(gericht["reaktion"]))
                        global last_action
                        last_action = {
                            'type': 'order',
                            'object': gericht
                        }
                        break
        elif re.compile("dank[eöi]+?", re.IGNORECASE).search(message.content) and (
                ("alois" in message.content.lower())):
            # print("Test")
            await client.send_message(message.channel, "Koin Problem :wink:")


client.run(discord_token)
